var express    = require('express'),
    app        = express(),
    cors       = require('cors'),
    mysql      = require('mysql'),
    bodyParser = require("body-parser");

//make connection to mysql
var client = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'apiRastreador',
  port:'3306',
  socketPath:'/Applications/MAMP/tmp/mysql/mysql.sock'
});

client.connect(function(err) {
  if (err) {
    console.error('error conectando: ' + err.stack);
    return;
  }
  console.log('conectado con ID ' + client.threadId);
});

// Middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());


var users = express.Router();
  function createUser(req,res){
    sql ="INSERT INTO `usuarios`(`id_usuario`, `nombre`, `rol`, `contrasena`, `nombre_empresa`, `idEmpresa`, `email`) VALUES (NULL,'"+ req.body.name+"',"+ req.body.rol+",'"+ req.body.password+"','"+ req.body.nombreEmpresa+"','1','"+ req.body.email+"')";
    client.query(sql,function(error,result){
      if (error) throw error;
      var json ={status:200};
      res.status(200).jsonp(json);
    });
  }
  users.route('/create').post(createUser);
var traking = express.Router();
  function saveTrack(req,res){
    sql ="INSERT INTO `trackRecords`(`idtrack`, `idUser`, `latitude`, `longitude`) VALUES (NULL,"+ req.body.idUser+","+req.body.latitude+","+ req.body.longitude+")";
    client.query(sql,function(error,result){
      if(error) throw error;
      var json = {status:200};
      res.status(200).jsonp(json);
    });
  }
  function getTracksbyUser(req,res){
    sql ="SELECT trackRecords.latitude AS userLatitude, trackRecords.longitude userLongitude, PuntosEntrega.latitude as pointLatitute, PuntosEntrega.longitude pointLongitude FROM trackRecords LEFT JOIN Rutas ON trackRecords.idUser = Rutas.idConductor LEFT JOIN PuntosEntrega ON Rutas.idPuntoEntrega = PuntosEntrega.id WHERE trackRecords.idUser = "+req.params.id +" ORDER BY trackRecords.idtrack DESC LIMIT 1";
    client.query(sql,function(error,resuls){
      if(error) throw error;
      var json = {status:200,positions:resuls};
      res.status(200).jsonp(json);
    });
  }

  function getTrackUsers(req,res){
    sql="SELECT Rutas.nombre as nombreRuta, Conductores.nombre AS nombreChofer, Mercancia.nombre AS Mercancia, vehiculos.noEconomico as vehiculo, Conductores.id AS driverId FROM Rutas LEFT JOIN Conductores ON Rutas.idConductor = Conductores.id LEFT JOIN PuntosEntrega ON Rutas.idPuntoEntrega = PuntosEntrega.id LEFT JOIN Mercancia ON Rutas.idMercancia = Mercancia.id LEFT JOIN vehiculos ON Rutas.idCamion = vehiculos.id";
    client.query(sql,function(error,resuls){
      if(error) throw error;
      var json = {status:200,results:resuls};
      res.status(200).jsonp(json);
    })
  }
  traking.route('/new').post(saveTrack);
  traking.route('/:id').get(getTracksbyUser);
  traking.route('/').get(getTrackUsers);
  
var drivers = express.Router();
  function createDriver(req,res){
    sql ="INSERT INTO `Conductores`(`id`, `nombre`, `telefono`, `licencia`, `foto`, `email`) VALUES (NULL,'"+ req.body.name +"','"+ req.body.telephone +"','"+ req.body.licence+"','','"+ req.body.email+"')";
    client.query(sql,function(error,result){
      if(error) throw error;
      var response={status:200};
      res.status(200).jsonp(response);
    });
  }
  function getDrivers(req,res){
    sql="SELECT * FROM Conductores";
    client.query(sql,function(error,results){
      if(error) throw error;
      var response = {status:200,results:results};
      res.status(200).jsonp(response);
    });
  }
  drivers.route('/create').post(createDriver);
  drivers.route('/get').get(getDrivers);
var alerts = express.Router();
  function createAlert(req,res){
    sql="INSERT INTO `Alertas`(`id`, `idConductor`, `hora`, `coordenadas`, `tipoAlerta`, `fecha`, `status`, `Mensaje`) VALUES (NULL,"+req.body.driverID+",NULL,'"+req.body.latitud + ","+ req.body.longitud+"',"+ req.body.alertType+","+ req.body.date+",0,'"+ req.body.message+"')";
    client.query(sql,function (error,results) {
      if(error) throw error;
      var reponse ={status:200};
      res.status(200).jsonp(reponse);
    });
  }
  function getAlerts(req,res){
    sql="SELECT * FROM Alertas LEFT JOIN Conductores on Alertas.idConductor = Conductores.id where status = 0";
    client.query(sql,function(error,results){
      var response = {status:200,results:results};
      res.status(200).jsonp(response);
    })
  }

  alerts.route('/create').post(createAlert);
  alerts.route('/get').get(getAlerts);
var productos = express.Router();

  function createProduct(req,res){
    sql ="INSERT INTO `Mercancia`(`id`, `nombre`, `descripcion`) VALUES (NULL,'"+ req.body.name+"','"+ req.body.descripcion+"')";
    client.query(sql,function(error,results){
      if(error) throw error;
      var resposne = {status:200};
      res.status(200).jsonp(resposne);
    });
  }
  function getProducts(req,res){
    sql="SELECT * FROM Mercancia";
    client.query(sql,function(error,results){
      if(error) throw error;
      var response ={status:200,results:results};
      res.status(200).jsonp(response);

    })
  }

  productos.route('/create').post(createProduct);
  productos.route('/get').get(getProducts);
var deliveryPoints = express.Router();
  function createPoint(req,res) {
    sql ="INSERT INTO `PuntosEntrega`(`id`, `direccion`, `latitude`, `longitude`, `horario`, `nombre`) VALUES (NULL,'"+req.body.address+"',"+ req.body.lat+","+ req.body.lng+",'"+req.body.hours+"','"+ req.body.name+"')";
    client.query(sql,function(error,results){
      if(error) throw error;
      var response = {status:200};
      res.status(200).jsonp(response);
    });
  }
  function getPoints(req,res){
    sql="SELECT * FROM PuntosEntrega";
    client.query(sql,function (error,results) {
      if(error) throw error;
      var response = {status:200,results:results};
      res.status(200).jsonp(response);
    })
  }
  deliveryPoints.route('/create').post(createPoint);
  deliveryPoints.route('/get').get(getPoints);
var routes = express.Router();
  function createRoute(req,res) {
    sql="INSERT INTO `Rutas`(`id`, `nombre`, `idCamion`, `idConductor`, `idPuntoEntrega`, `idMercancia`) VALUES (NULL,'"+ req.body.name+"',"+req.body.truckId+","+ req.body.driverId+","+ req.body.deliveryId+","+req.body.productId+")";
    client.query(sql,function(error,results){
      if(error) throw error;
      var response = {status:200,results:results};
      res.status(200).jsonp(response);
    })
  }
  function getRoutes(req,res) {
    sql="SELECT * FROM Rutas";
    client.query(sql,function(error,results){
      if(error) throw error;
      var response ={status:200,results:results};
      res.status(200).jsonp(response);
    });
  }
  routes.route('/create').post(createRoute);
  routes.route('/get').get(getRoutes);
var vehicules = express.Router();
  function createVehicule(req,res) {
    sql ="INSERT INTO `vehiculos`(`id`, `placas`, `modelo`, `marca`, `noEconomico`, `noSeguro`, `descripcion`, `foto`) VALUES (NULL,'"+ req.body.placas+"','"+req.body.modelo+"','"+ req.body.marca+"','"+req.body.noEconomico+"','"+ req.body.noSeguro+"','"+req.body.descripcion+"','')";
    client.query(sql,function (error,results) {
      if(error) throw error;
      var response = {status:200,results:results};
      res.status(200).jsonp(response);
    })
  }
  function getVehicules(req,res) {
    sql="SELECT * FROM vehiculos";
    client.query(sql,function(error,results){
      if(error) throw error;
      var response={status:200,results:results};
      res.status(200).jsonp(response);
    });
  }
  vehicules.route('/create').post(createVehicule);
  vehicules.route('/get').get(getVehicules);
//Routes
  app.use('/vehiculos',vehicules);
  app.use('/routes',routes);
  app.use('/points',deliveryPoints);
  app.use('/productos',productos);
  app.use('/alerts',alerts);
  app.use('/drivers',drivers);
  app.use('/users',users);
  app.use('/track',traking);
// Start server
app.listen(4000, function() {
  console.log("Node server running on http://localhost:4000");
});
